//
//  ProgramSelectionViewModelTests.m
//  SweatTests
//
//  Created by Saurabh Verma on 15/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ProgramSelectionViewModel.h"

#pragma mark - Tests Scenarios

@protocol ProgramSelectionViewModelTestsScenarios <NSObject>

// Test `screenDisplayModel` Property
- (void)testScreenDisplayModel;

// Test `programDisplayModels` Property
- (void)testProgramDisplayModels_Empty;
- (void)testProgramDisplayModels_WithValues;


// Test `- (void)viewShown` Function
- (void)testViewShown_Success;
- (void)testViewShown_Failure;

// Test `- (void)willDisplayCellWithIndex:(NSUInteger)index Function`
- (void)testWillDisplayCellWithIndex;

// Test `- (void)didSelectCellWithIndex:(NSUInteger)index`
- (void)testDidSelectCellWithIndex;

@end

#pragma mark - Stubs Declaration

@interface ViewController : NSObject <ProgramSelectionViewModelDelegate>

@property(nonatomic, assign) int timesDidUpdateProgramsWithCalled;
@property(nonatomic, assign) int timesDidFailToUpdateProgramsWith;
@property(nonatomic, assign) int timesDidUpdateProgramImageWithIndex;
@property(nonatomic, strong) NSString* errorMessage;

@end

@interface AssetDownloadClientStub: NSObject <AssetDownloadClientType>

@property(nonatomic, strong) NSString* argumentURL;

@end

@interface ProgramSelectionClientStub: NSObject <ProgramSelectionClientType>

@property(nonatomic, assign) bool shouldReturnSuccess;

@end

@interface ProgramSelectionRouterStub : NSObject <ProgramSelectionRouterType>

@property(nonatomic, assign) int timesPresentProgramDetailScreenWithCalled;

@end

@interface ProgramSelectionViewModelTests : XCTestCase <ProgramSelectionViewModelTestsScenarios>

@property(nonatomic, strong) ViewController* viewController;
@property(nonatomic, strong) AssetDownloadClientStub* assetDownloadClient;
@property(nonatomic, strong) ProgramSelectionClientStub* programSelectionClient;
@property(nonatomic, strong) ProgramSelectionRouterStub* router;
@property(nonatomic, strong) ProgramSelectionViewModel* viewModel;

@end

@implementation ProgramSelectionViewModelTests

@synthesize viewController;
@synthesize assetDownloadClient;
@synthesize programSelectionClient;
@synthesize router;
@synthesize viewModel;

- (void)setUp {
    
    viewController = [[ViewController alloc] init];
    assetDownloadClient = [[AssetDownloadClientStub alloc] init];
    programSelectionClient = [[ProgramSelectionClientStub alloc] init];
    router = [[ProgramSelectionRouterStub alloc] init];
    
    viewModel = [[ProgramSelectionViewModel alloc] initWithProgramSelectionClient: programSelectionClient
                                                              assetDownloadClient: assetDownloadClient
                                                                           router: router];
    
    viewModel.delegate = viewController;

}

- (void)tearDown {
    
    viewController = nil;
    assetDownloadClient = nil;
    programSelectionClient = nil;
    router = nil;
    viewModel = nil;

}

#pragma mark - ProgramSelectionViewModelTestsScenarios Conformance

- (void)testScreenDisplayModel {
    
    // Given view model is initialised
    // Initialised in `- (void)setUp`
    
    // When
    ProgramScreenDisplayModel* screenDisplayModel = viewModel.screenDisplayModel;
    
    // Then
    XCTAssertTrue(screenDisplayModel != nil);
    XCTAssertTrue([screenDisplayModel.screenLogo isEqualToString:@"sweat-logo"]);
    XCTAssertTrue([screenDisplayModel.screenTitle isEqualToString:@"Program Suggestions"]);
    
}

- (void)testProgramDisplayModels_Empty {

    // Given view model is initialised
    // Initialised in `- (void)setUp`
    
    // When
    NSArray<ProgramDisplayModel*>* displayModels = viewModel.programDisplayModels;
    
    // Then
    XCTAssertTrue(displayModels.count == 0);
    
}

- (void)testProgramDisplayModels_WithValues {
    
    // Given view model is initialised
    // Initialised in `- (void)setUp`
    
    // When
    programSelectionClient.shouldReturnSuccess = true;
    [viewModel viewShown];
    NSArray<ProgramDisplayModel*>* displayModels = viewModel.programDisplayModels;
    
    // Then
    XCTAssertTrue(displayModels.count != 0);
    
}

- (void)testViewShown_Success {
   
    // Given view model is initialised
    // Initialised in `- (void)setUp`
    
    // When
    programSelectionClient.shouldReturnSuccess = true;
    [viewModel viewShown];
    
    // Then
    XCTAssertTrue(viewModel.programDisplayModels.count == 2);
    
    // AND
    XCTAssertTrue(viewController.timesDidUpdateProgramsWithCalled == 1);
    
    // AND
    XCTAssertTrue([viewModel.programDisplayModels[0].nameText isEqualToString:@"BBG"]);
    XCTAssertTrue([viewModel.programDisplayModels[0].trainWithText isEqualToString:@"with Kayla Itsines"]);
    XCTAssertTrue([viewModel.programDisplayModels[0].imageURL isEqualToString:@"https://assets.sweat.com/workout_groups/images/000/000/001/original/bbg-1242x2208593b84bcce1c6835be6cf215d7a01832.png?1542659076"]);
    XCTAssertTrue(viewModel.programDisplayModels[0].imageData == nil);
    
    
    XCTAssertTrue(viewModel.programDisplayModels[0].intensityImageNames.count == 5);
    XCTAssertTrue([viewModel.programDisplayModels[0].intensityImageNames[0] isEqualToString:@"sweat-drop-filled"]);
    XCTAssertTrue([viewModel.programDisplayModels[0].intensityImageNames[2] isEqualToString:@"sweat-drop-filled"]);
    XCTAssertTrue([viewModel.programDisplayModels[0].intensityImageNames[4] isEqualToString:@"sweat-drop"]);
    
    XCTAssertTrue(viewModel.programDisplayModels[0].attributes.count == 1);
    XCTAssertTrue([viewModel.programDisplayModels[0].attributes[0].nameText isEqualToString:@"Weight Loss"]);
    XCTAssertTrue(viewModel.programDisplayModels[0].attributes[0].value == 4 / (float)5);
    
    XCTAssertTrue(viewModel.programDisplayModels[0].tags.count == 2);
    
}

- (void)testViewShown_Failure {
 
    // Given view model is initialised
    // Initialised in `- (void)setUp`
    
    // When
    programSelectionClient.shouldReturnSuccess = false;
    [viewModel viewShown];
    
    // Then
    XCTAssertTrue(viewModel.programDisplayModels.count == 0);
    
    // AND
    XCTAssertTrue(viewController.timesDidFailToUpdateProgramsWith == 1);
    
    // AND
    XCTAssertTrue(viewController.errorMessage != nil);
    
}

- (void)testWillDisplayCellWithIndex {
    
    // Given view model is initialised
    // Initialised in `- (void)setUp`
    
    // When
    programSelectionClient.shouldReturnSuccess = true;
    [viewModel viewShown];
    [viewModel willDisplayCellWithIndex: 1];
    
    // Then
    XCTAssertTrue([assetDownloadClient.argumentURL isEqualToString:@"https://assets.sweat.com/workout_groups/images/000/000/003/original/bam-1242x2208e8a422a719bd8a84d91e84d24edbae54.png?1542659114"]);
    
    // And
    XCTAssertTrue(viewModel.programDisplayModels[1].imageData != nil);
    
    // And
    XCTAssertTrue(viewController.timesDidUpdateProgramImageWithIndex == 1);
    
}

- (void)testDidSelectCellWithIndex {
    
    // Given view model is initialised
    // Initialised in `- (void)setUp`
    
    // When
    programSelectionClient.shouldReturnSuccess = true;
    [viewModel viewShown];
    [viewModel didSelectCellWithIndex: 1];
    
    // Then
    XCTAssertTrue(router.timesPresentProgramDetailScreenWithCalled == 1);
    
}

@end

#pragma mark - Stubs Definition

@implementation ViewController
@synthesize timesDidFailToUpdateProgramsWith;
@synthesize timesDidUpdateProgramsWithCalled;
@synthesize timesDidUpdateProgramImageWithIndex;
@synthesize errorMessage;

#pragma mark - ProgramSelectionViewModelDelegate Conformance

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.timesDidUpdateProgramsWithCalled = 0;
        self.timesDidFailToUpdateProgramsWith = 0;
        self.timesDidUpdateProgramImageWithIndex = 0;
    }
    return self;
}

- (void)didUpdateProgramsWith:(NSArray<ProgramDisplayModel *> *)displayModels {
    
    timesDidUpdateProgramsWithCalled += 1;
    
}

- (void)didFailToUpdateProgramsWith:(NSString *)errorMessage {
    
    self.errorMessage = errorMessage;
    timesDidFailToUpdateProgramsWith += 1;
    
}

- (void)didUpdateProgramImageWithIndex:(NSUInteger)index {
    
    timesDidUpdateProgramImageWithIndex += 1;

}

@end

@implementation AssetDownloadClientStub
@synthesize argumentURL;

- (void)downloadImageWithUrl:(nonnull NSString *)url completion:(nonnull void (^)(NSData * _Nullable))completion {
    
    self.argumentURL = url;
    completion([NSData data]);

}

@end

@implementation ProgramSelectionClientStub
@synthesize shouldReturnSuccess;

- (void)programsFromResource:(nonnull NSString *)resource withCompletion:(nonnull ProgramResponse)completion {
    
    NSError* error = nil;
    
    if (shouldReturnSuccess) {
        completion([self samplePrograms], error);
    } else {
        error = [NSError errorWithDomain: @"ErrorDomain"
                                    code: 1001
                                userInfo: @{@"JSONError": @"Bad json"}];
        completion(nil, error);
    }
    
}

- (NSArray<Program*>*)samplePrograms {
    
    NSMutableArray <Program*> *programs = [NSMutableArray array];
    
    Program* program1 = [[Program alloc] initWithId: 1
                                             acronym: @"BBG"
                                            codeName: @"kayla"
                                                name: @"BBG"
                                            imageURL: @"https://assets.sweat.com/workout_groups/images/000/000/001/original/bbg-1242x2208593b84bcce1c6835be6cf215d7a01832.png?1542659076"
                                          attributes: [self sampleAttributes]
                                                tags: [self sampleTags]
                                             trainer: [self sampleTrainer]];
                        
    [programs addObject: program1];
    
    Program* program2 = [[Program alloc] initWithId: 1
                                            acronym: nil
                                           codeName: @"yoga"
                                               name: @"Body and Mind"
                                           imageURL: @"https://assets.sweat.com/workout_groups/images/000/000/003/original/bam-1242x2208e8a422a719bd8a84d91e84d24edbae54.png?1542659114"
                                         attributes: [self sampleAttributes]
                                               tags: [self sampleTags]
                                            trainer: [self sampleTrainer]];
    
    [programs addObject: program2];
    
    return programs;

}

- (NSArray<Attributes*>*)sampleAttributes {
    
    NSMutableArray <Attributes*> *attributes = [NSMutableArray array];
    
    Attributes* attribute1 = [[Attributes alloc] initWithId: 1
                                                       code: AttributeCodeIntensity
                                                       name: @"Intensity"
                                                      value: 3
                                                      total: 5];
    [attributes addObject:attribute1];
    
    Attributes* attribute2 = [[Attributes alloc] initWithId: 2
                                                       code: AttributeCodeDefault
                                                       name: @"Weight Loss"
                                                      value: 4
                                                      total: 5];
    [attributes addObject:attribute2];
    
    return attributes;

}

- (NSArray<Tag*>*)sampleTags {
    
    NSMutableArray <Tag*> *tags = [NSMutableArray array];
    
    Tag* tag1 = [[Tag alloc] initWithId: 129 name: @"Equipment Needed"];
    [tags addObject:tag1];
    
    Tag* tag2 = [[Tag alloc] initWithId: 127 name: @"Gym"];
    [tags addObject:tag2];
    
    return tags;

}

- (Trainer*)sampleTrainer {
    
    return [[Trainer alloc] initWithId: 2
                              codeName: @"kayla"
                                  name: @"Kayla Itsines"
                              imageURL: @"https://assets.sweat.com/trainers/images/000/000/002/original/BBG_Stronger_3x.png?1521524134"];
    
}

@end

@implementation ProgramSelectionRouterStub
@synthesize timesPresentProgramDetailScreenWithCalled;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.timesPresentProgramDetailScreenWithCalled = 0;
    }
    return self;
}

- (void)presentProgramDetailScreenWith:(ProgramDisplayModel *)programDisplayModel {
    
    timesPresentProgramDetailScreenWithCalled += 1;

}

@end
