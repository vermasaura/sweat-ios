//
//  ProgramSelectionClientTests.m
//  SweatTests
//
//  Created by Saurabh Verma on 16/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ProgramSelectionClient.h"
#import "NSBundle+BundleProtocol.h"

@protocol ProgramSelectionClientTestsScenarios <NSObject>

// Test `- (void)programsFromResource:(NSString*)resource withCompletion: (ProgramResponse)completion` Function
- (void)testProgramsFromResource_Success;
- (void)testProgramsFromResource_Failure_ResourceNA;
- (void)testProgramsFromResource_Failure_BadJSON;

@end

@interface ProgramSelectionClientTests : XCTestCase <ProgramSelectionClientTestsScenarios>

@property(nonatomic, strong) NSBundle *bundle;
@property(nonatomic, strong) ProgramSelectionClient* client;

@end

@implementation ProgramSelectionClientTests
@synthesize client;
@synthesize bundle;

- (void)setUp {

    bundle = [NSBundle bundleForClass:[self class]];
    client = [[ProgramSelectionClient alloc] initWithBundle: bundle];
    
}

- (void)tearDown {

    bundle = nil;
    client = nil;
    
}

#pragma mark - ProgramSelectionClientTestsScenarios Conformance

- (void)testProgramsFromResource_Success {
    
    // Given client is initialised
    // Initialised in `- (void)setUp`
    
    // When
    __block NSArray<Program *> *programs = nil;
    __block NSError* jsonError = nil;
    
    XCTestExpectation* expectation = [self expectationWithDescription: @"programsFromResource"];

    [client programsFromResource: @"trainer-programs-success"
                  withCompletion:^(NSArray<Program *> * _Nullable result, NSError * _Nullable error) {
                      programs = result;
                      jsonError = error;
                      
                      [expectation fulfill];
                  }];
    
    
    [self waitForExpectationsWithTimeout:5.0 handler: nil];

    // Then
    XCTAssertTrue(programs != nil);
    XCTAssertTrue(jsonError == nil);
    
    // And
    XCTAssertTrue(programs.count == 7);
    XCTAssertTrue(programs[0].identifier == 1);
    XCTAssertTrue([programs[0].acronym isEqualToString:@"BBG"]);
    XCTAssertTrue([programs[0].codeName isEqualToString:@"kayla"]);
    XCTAssertTrue([programs[0].name isEqualToString:@"BBG"]);
    XCTAssertTrue([programs[0].imageURL isEqualToString:@"https://assets.sweat.com/workout_groups/images/000/000/001/original/bbg-1242x2208593b84bcce1c6835be6cf215d7a01832.png?1542659076"]);
    
    XCTAssertTrue(programs[0].attributes.count == 5);
    XCTAssertTrue(programs[0].attributes[0].identifier == 1);
    XCTAssertTrue(programs[0].attributes[0].code == AttributeCodeIntensity);
    XCTAssertTrue([programs[0].attributes[0].name isEqualToString:@"Intensity"]);
    XCTAssertTrue(programs[0].attributes[0].value == 3);
    XCTAssertTrue(programs[0].attributes[0].total == 5);
    
    XCTAssertTrue(programs[0].tags.count == 8);
    XCTAssertTrue(programs[0].tags[0].identifier == 129);
    XCTAssertTrue([programs[0].tags[0].name isEqualToString:@"Equipment Needed"]);
    
    XCTAssertTrue(programs[0].trainer.identifier == 2);
    XCTAssertTrue([programs[0].trainer.name isEqualToString:@"Kayla Itsines"]);
    XCTAssertTrue([programs[0].trainer.codeName isEqualToString:@"kayla"]);
    XCTAssertTrue([programs[0].trainer.imageURL isEqualToString:@"https://assets.sweat.com/trainers/images/000/000/002/original/BBG_Stronger_3x.png?1521524134"]);
    
}

- (void)testProgramsFromResource_Failure_ResourceNA {
    
    // Given client is initialised
    // Initialised in `- (void)setUp`
    
    // When
    __block NSArray<Program *> *programs = nil;
    __block NSError* parseError = nil;
    XCTestExpectation* expectation = [self expectationWithDescription: @"programsFromResource"];
    
    [client programsFromResource: @"NA"
                  withCompletion:^(NSArray<Program *> * _Nullable result, NSError * _Nullable error) {
                      programs = result;
                      parseError = error;
                      
                      [expectation fulfill];
                  }];
    
    [self waitForExpectationsWithTimeout:5.0 handler: nil];
    
    // Then
    XCTAssertTrue(programs == nil);
    XCTAssertTrue(parseError != nil);
    
    // And
    XCTAssertTrue([parseError.domain isEqualToString:@"ResourceNotFound"]);
    XCTAssertTrue(parseError.code == 1001);
    XCTAssertTrue([parseError.userInfo[@"ErrorMesaage"] isEqualToString:@"Resource not found"]);
    
}

- (void)testProgramsFromResource_Failure_BadJSON {
    
    // Given client is initialised
    // Initialised in `- (void)setUp`
    
    // When
    __block NSArray<Program *> *programs = nil;
    __block NSError* parseError = nil;
    XCTestExpectation* expectation = [self expectationWithDescription: @"programsFromResource"];
    
    [client programsFromResource: @"trainer-programs-failure"
                  withCompletion:^(NSArray<Program *> * _Nullable result, NSError * _Nullable error) {
                      programs = result;
                      parseError = error;
                      
                      [expectation fulfill];
                  }];
    
    [self waitForExpectationsWithTimeout:5.0 handler: nil];
    
    // Then
    XCTAssertTrue(programs == nil);
    XCTAssertTrue(parseError != nil);
    
    // And
    XCTAssertTrue([parseError.domain isEqualToString:@"BadJSON"]);
    XCTAssertTrue(parseError.code == 1003);
    XCTAssertTrue([parseError.userInfo[@"ErrorMesaage"] isEqualToString:@"Coudn't read JSON from data"]);
    
}

@end
