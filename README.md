# Sweat

## Environmental setup
- macOS version: Mojave 10.14
- Xcode version: 10.1

### Project setup

To run the project, clone the repo using command
```bash
git clone https://vermasaura@bitbucket.org/vermasaura/sweat-ios.git
```

## Assumptions
- To have the UI behave correctly, exact 5 `Attributes` would return from the API for each `Program`. One of them is `Insensity`. This assumption was made looking at the JSON provided.
- The total value for `Intensity` attribute will be exact `5`
- The image shown on each cell is `Program.image` not `Program.trainer.image_url`.

## Further Improvements / Tech Debts
- Localise all the strings
- Define ErrorCodes globally
- Rather than returning `imageData` in `AssetDownloadClient`, move the image from `tmp` to `Library/Caches/Program_Images`. As of now images are taking 2X memory, one is on the `imageview` on UI and other places is `imageData` inside `ProgramDisplayModel`.

## Architecture, Design Patterns and Other Documentation

The project has been made using Test-driven-development methodology where-in User Stories are mapped to unit tests first and then code is written in class being tested.

Unit tests uses protocol oriented programming to have all the test scenarios defined as rules on the top of `XCTestCase` class. The unit tests enjoys the use of dependency injection to inject stubs / mocks in the class being tested.

The projects also uses protocol oriented programming and dependency injection to make the classes loosely coupled with other classes and makes the code 100% testable.

The properties are immutable throughout the project to avoid side effects state changes.

Ideally, views / view controller doesn't know about the models. Hence, display models are being used to map the data from models to UI.

`Router` is being used to keep the presentation logic out of the view controller.

`Xib`s are used instead of storyboard so that view controllers dependencies can be injected in the `init`.

The following concepts are used throughout the codebase:

- [Protocol-Oriented with BDD](https://dzone.com/articles/introducing-protocol-oriented-bdd-in-swift-for-ios)
- [MVVM + Router](https://medium.com/commencis/routing-with-mvvm-on-ios-f22d021ad2b2)
- [Dependency Injection](https://cocoacasts.com/nuts-and-bolts-of-dependency-injection-in-swift)
