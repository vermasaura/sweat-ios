//
//  Tag.h
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Tag : NSObject

@property (readonly) NSUInteger identifier;
@property (readonly) NSString* name;

- (instancetype)initWithId:(NSUInteger)identifier name:(NSString*)name;

@end

NS_ASSUME_NONNULL_END
