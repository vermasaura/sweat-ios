//
//  Attributes.m
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import "Attributes.h"

@interface Attributes()

@property (nonatomic) NSUInteger identifier;
@property (nonatomic, strong) NSString* codeName;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, assign) NSUInteger value;
@property (nonatomic, assign) NSUInteger total;

@end

@implementation Attributes

-(instancetype)initWithId:(NSUInteger)identifier
                 codeName:(NSString* _Nullable)codeName
                     name:(NSString* _Nullable)name
                    value:(NSUInteger)value
                    total:(NSUInteger)total {
    
    self = [super init];
    
    if (self) {
        self.identifier = identifier;
        self.codeName = codeName;
        self.name = name;
        self.value = value;
        self.total = total;
    }
    
    return self;
    
}

@end
