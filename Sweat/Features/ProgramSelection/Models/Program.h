//
//  Program.h
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tag : NSObject

@property (readonly) NSUInteger identifier;
@property (readonly) NSString* name;

- (instancetype)initWithId:(NSUInteger)identifier name:(NSString*)name;

@end

@interface Trainer : NSObject

@property (readonly) NSUInteger identifier;
@property (readonly) NSString* codeName;
@property (readonly) NSString* name;
@property (readonly) NSString* imageURL;

- (instancetype)initWithId:(NSUInteger)identifier
                  codeName:(NSString* _Nullable)codeName
                      name:(NSString* _Nullable)name
                  imageURL:(NSString* _Nullable)imageURL;

@end

typedef NS_ENUM(NSUInteger, AttributeCode) {
  
    AttributeCodeIntensity,
    AttributeCodeDefault
    
};

@interface Attributes : NSObject

@property (readonly) NSUInteger identifier;
@property (readonly) AttributeCode code;
@property (readonly) NSString* name;
@property (readonly) NSUInteger value;
@property (readonly) NSUInteger total;

-(instancetype)initWithId:(NSUInteger)identifier
                     code:(AttributeCode)code
                     name:(NSString* _Nullable)name
                    value:(NSUInteger)value
                    total:(NSUInteger)total;

@end

@interface Program : NSObject

@property (readonly) NSUInteger identifier;
@property (readonly) NSString* acronym;
@property (readonly) NSString* codeName;
@property (readonly) NSString* name;
@property (readonly) NSString* imageURL;
@property (readonly) NSArray<Attributes *>* attributes;
@property (readonly) NSArray<Tag *>* tags;
@property (readonly) Trainer *trainer;

- (instancetype)initWithId:(NSUInteger)identifier
                acronym:(NSString* _Nullable)acronym
                codeName:(NSString* _Nullable)codeName
                name:(NSString* _Nullable)name
                imageURL:(NSString* _Nullable)imageURL
                attributes:(NSArray<Attributes *>* _Nullable)attributes
                tags:(NSArray<Tag *>* _Nullable)tags
                trainer:(Trainer* _Nullable)trainer;

@end
