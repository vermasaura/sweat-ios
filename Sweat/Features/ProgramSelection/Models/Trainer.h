//
//  Trainer.h
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Trainer : NSObject

@property (readonly) NSUInteger identifier;
@property (readonly) NSString* codeName;
@property (readonly) NSString* name;
@property (readonly) NSString* imageUrl;

- (instancetype)initWithId:(NSUInteger)identifier
                codeName:(NSString* _Nullable)codeName
                    name:(NSString* _Nullable)name
                imageURL:(NSString* _Nullable)imageURL;

@end

NS_ASSUME_NONNULL_END
