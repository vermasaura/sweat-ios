//
//  Trainer.m
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import "Trainer.h"

@interface Trainer()

@property (nonatomic) NSUInteger identifier;
@property (nonatomic, strong) NSString* codeName;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* imageURL;

@end

@implementation Trainer

- (instancetype)initWithId:(NSUInteger)identifier
                codeName:(NSString* _Nullable)codeName
                    name:(NSString* _Nullable)name
                imageURL:(NSString* _Nullable)imageURL {
    
    self = [super init];
    
    if (self) {
        self.identifier = identifier;
        self.codeName = codeName;
        self.name = name;
        self.imageURL = imageURL;
    }
    
    return self;

}

@end
