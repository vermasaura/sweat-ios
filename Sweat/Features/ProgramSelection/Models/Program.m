//
//  Program.m
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import "Program.h"

@interface Tag()

@property (nonatomic) NSUInteger identifier;
@property (nonatomic, strong) NSString* name;

@end

@implementation Tag

- (instancetype)initWithId:(NSUInteger)identifier name:(NSString*)name {
    
    self = [super init];
    
    if (self) {
        self.identifier = identifier;
        self.name = name;
    }
    
    return self;
    
}

@end

@interface Trainer()

@property (nonatomic) NSUInteger identifier;
@property (nonatomic, strong) NSString* codeName;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* imageURL;

@end

@implementation Trainer

- (instancetype)initWithId:(NSUInteger)identifier
                  codeName:(NSString* _Nullable)codeName
                      name:(NSString* _Nullable)name
                  imageURL:(NSString* _Nullable)imageURL {
    
    self = [super init];
    
    if (self) {
        self.identifier = identifier;
        self.codeName = codeName;
        self.name = name;
        self.imageURL = imageURL;
    }
    
    return self;
    
}

@end

@interface Attributes()

@property (nonatomic) NSUInteger identifier;
@property (nonatomic, assign) AttributeCode code;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, assign) NSUInteger value;
@property (nonatomic, assign) NSUInteger total;

@end

@implementation Attributes

-(instancetype)initWithId:(NSUInteger)identifier
                     code:(AttributeCode)code
                     name:(NSString* _Nullable)name
                    value:(NSUInteger)value
                    total:(NSUInteger)total {
    
    self = [super init];
    
    if (self) {
        self.identifier = identifier;
        self.code = code;
        self.name = name;
        self.value = value;
        self.total = total;
    }
    
    return self;
    
}

@end

@interface Program()

@property (nonatomic) NSUInteger identifier;
@property (nonatomic, strong) NSString* acronym;
@property (nonatomic, strong) NSString* codeName;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* imageURL;
@property (nonatomic, strong) NSArray<Attributes *>* attributes;
@property (nonatomic, strong) NSArray<Tag *>* tags;
@property (nonatomic, strong) Trainer *trainer;

@end

@implementation Program

- (instancetype)initWithId:(NSUInteger)identifier
                acronym:(NSString* _Nullable)acronym
                codeName:(NSString* _Nullable)codeName
                name:(NSString* _Nullable)name
                imageURL:(NSString* _Nullable)imageURL
                attributes:(NSArray<Attributes *>* _Nullable)attributes
                tags:(NSArray<Tag *>* _Nullable)tags
                trainer:(Trainer* _Nullable)trainer {
    
    self = [super init];
                    
    if (self) {
        self.identifier = identifier;
        self.acronym = acronym;
        self.codeName = codeName;
        self.name = name;
        self.imageURL = imageURL;
        self.attributes = attributes;
        self.tags = tags;
        self.trainer = trainer;
    }
    
    return self;
    
}

@end
