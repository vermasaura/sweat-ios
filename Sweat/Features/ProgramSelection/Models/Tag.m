//
//  Tag.m
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import "Tag.h"

@interface Tag()

@property (nonatomic) NSUInteger identifier;
@property (nonatomic, strong) NSString* name;

@end

@implementation Tag

- (instancetype)initWithId:(NSUInteger)identifier name:(NSString*)name {
    
    self = [super init];
    
    if (self) {
        self.identifier = identifier;
        self.name = name;
    }
    
    return self;
    
}

@end
