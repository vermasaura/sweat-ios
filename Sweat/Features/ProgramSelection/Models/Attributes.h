//
//  Attributes.h
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Attributes : NSObject

@property (readonly) NSUInteger identifier;
@property (readonly) NSString* codeName;
@property (readonly) NSString* name;
@property (readonly) NSUInteger value;
@property (readonly) NSUInteger total;

-(instancetype)initWithId:(NSUInteger)identifier
                 codeName:(NSString* _Nullable)codeName
                     name:(NSString* _Nullable)name
                    value:(NSUInteger)value
                    total:(NSUInteger)total;

@end

NS_ASSUME_NONNULL_END
