//
//  ProgramSelectionViewController.m
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import "ProgramSelectionViewController.h"
#import "ProgramSelectionTableViewCell.h"
#import "Theme.h"

@interface ProgramSelectionViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView* tableView;
@property (nonatomic, retain) id<ProgramSelectionViewModelType> viewModel;

@end

@implementation ProgramSelectionViewController

@synthesize viewModel;
@synthesize tableView;

#pragma mark - Initialization with dependency injection
- (instancetype)initWithViewModel:( id<ProgramSelectionViewModelType> )viewModel {
    
    self = [super init];
    
    if (self) {
     self.viewModel = viewModel;
    }

    return self;

}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self configureHeader];
    [self.tableView registerNib:[UINib nibWithNibName: NSStringFromClass(ProgramSelectionTableViewCell.self)
                                               bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier: NSStringFromClass(ProgramSelectionTableViewCell.self)];
    
    [viewModel viewShown];
    
}

- (void)viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    [self configureTableHeader];
    
}

#pragma mark - Private
#pragma mark Functions

- (void)configureHeader {
    
    ProgramScreenDisplayModel* displayModel = viewModel.screenDisplayModel;
    UIImage *headerLogo = [UIImage imageNamed:displayModel.screenLogo];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:headerLogo];
    
}

- (void)configureTableHeader {
    
    UILabel* tableHeaderTitleView = [[UILabel alloc] init];
    tableHeaderTitleView.font = [UIFont fontWithName:[UIFont montserratBoldFont] size: 24.0];
    tableHeaderTitleView.text = viewModel.screenDisplayModel.screenTitle;
    
    UIView* tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 80.0)];
    
    tableHeaderTitleView.translatesAutoresizingMaskIntoConstraints = false;
    [tableHeaderView addSubview:tableHeaderTitleView];
    [[tableHeaderTitleView.leadingAnchor constraintEqualToAnchor:tableHeaderView.leadingAnchor constant: 16.0] setActive: true];
    [[tableHeaderTitleView.trailingAnchor constraintEqualToAnchor:tableHeaderView.trailingAnchor] setActive: true];
    [[tableHeaderTitleView.topAnchor constraintEqualToAnchor:tableHeaderView.topAnchor] setActive: true];
    [[tableHeaderTitleView.bottomAnchor constraintEqualToAnchor:tableHeaderView.bottomAnchor] setActive: true];
    
    tableView.tableHeaderView = tableHeaderView;

}

#pragma mark - Delegates
#pragma mark ProgramSelectionViewModelDelegate

- (void)didUpdateProgramsWith:( NSArray<ProgramDisplayModel*> *)displayModels {
    
    [tableView reloadData];

}

- (void)didFailToUpdateProgramsWith:(NSString *)errorMessage {
    
    // Show Empty Error Screen with Retry button
    
}

- (void)didUpdateProgramImageWithIndex:(NSUInteger)index {
    
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [tableView reloadRowsAtIndexPaths: @[indexPath] withRowAnimation: UITableViewRowAnimationNone];
    
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return viewModel.programDisplayModels.count;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProgramSelectionTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: NSStringFromClass(ProgramSelectionTableViewCell.self)];
    
    [cell configureWithDisplayModel: viewModel.programDisplayModels[indexPath.row]];
    
    return cell;
    
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    [viewModel willDisplayCellWithIndex: indexPath.row];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath: indexPath animated: true];
    [viewModel didSelectCellWithIndex: indexPath.row];

}

@end

