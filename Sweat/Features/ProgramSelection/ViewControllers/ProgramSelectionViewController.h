//
//  ProgramSelectionViewController.h
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProgramSelectionViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProgramSelectionViewController : UIViewController <ProgramSelectionViewModelDelegate>

- (instancetype)initWithViewModel:( id<ProgramSelectionViewModelType> )viewModel;

@end

NS_ASSUME_NONNULL_END
