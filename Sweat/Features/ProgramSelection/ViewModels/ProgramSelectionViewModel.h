//
//  ProgramSelectionViewModel.h
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProgramSelectionClient.h"
#import "ProgramDisplayModel.h"
#import "AssetDownloadClient.h"
#import "ProgramSelectionRouter.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ProgramSelectionViewModelType <NSObject>

@property(nonatomic, strong, readonly) ProgramScreenDisplayModel* screenDisplayModel;
@property(nonatomic, strong, readonly) NSArray<ProgramDisplayModel*> * programDisplayModels;

- (void)viewShown;
- (void)willDisplayCellWithIndex:(NSUInteger)index;
- (void)didSelectCellWithIndex:(NSUInteger)index;

@end

@protocol ProgramSelectionViewModelDelegate <NSObject>

- (void)didUpdateProgramsWith:( NSArray<ProgramDisplayModel*> *)displayModels;
- (void)didFailToUpdateProgramsWith:(NSString *)errorMessage;
- (void)didUpdateProgramImageWithIndex: (NSUInteger)index;

@end

@interface ProgramSelectionViewModel : NSObject <ProgramSelectionViewModelType>

@property (nonatomic, weak) id<ProgramSelectionViewModelDelegate> delegate;

- (instancetype)initWithProgramSelectionClient: ( id<ProgramSelectionClientType> )programSelectionClient
                           assetDownloadClient: ( id<AssetDownloadClientType> ) assetDownloadClient
                                        router: ( id<ProgramSelectionRouterType> )router;

@end

NS_ASSUME_NONNULL_END
