//
//  ProgramSelectionViewModel.m
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import "ProgramSelectionViewModel.h"
#import "Program.h"

@interface ProgramSelectionViewModel()

@property(readwrite) ProgramScreenDisplayModel* screenDisplayModel;
@property(nonatomic, strong) id<ProgramSelectionClientType> programSelectionClient;
@property(nonatomic, strong) id<AssetDownloadClientType> assetDownloadClient;
@property(nonatomic, strong) id<ProgramSelectionRouterType> router;
@property(nonatomic, strong) NSArray<ProgramDisplayModel*> * displayModels;

@end

@implementation ProgramSelectionViewModel

@synthesize programSelectionClient;
@synthesize assetDownloadClient;
@synthesize router;
@synthesize displayModels;

#pragma mark - Initialization

- (instancetype)initWithProgramSelectionClient: ( id<ProgramSelectionClientType> )programSelectionClient
                           assetDownloadClient: ( id<AssetDownloadClientType> ) assetDownloadClient
                                        router: ( id<ProgramSelectionRouterType> )router {
    
    self = [super init];
    if (self) {
        self.programSelectionClient = programSelectionClient;
        self.assetDownloadClient = assetDownloadClient;
        self.router = router;
        
        self.screenDisplayModel = [[ProgramScreenDisplayModel alloc] initWithScreenLogo:@"sweat-logo"
                                                                            screenTitle:@"Program Suggestions"];
        
    }
    return self;
    
}

#pragma mark ProgramSelectionDisplayModel Conformance

- (NSArray<ProgramDisplayModel *> *)programDisplayModels {
    
    return displayModels;
    
}

- (void)viewShown {
    
    [self programSelections];

}

- (void)willDisplayCellWithIndex:(NSUInteger)index {
    
    if (displayModels[index].imageData == nil) {
    
        [self imageDataForIndex: index];

    }
    
}

- (void)didSelectCellWithIndex:(NSUInteger)index {
    
    [router presentProgramDetailScreenWith: displayModels[index]];

}

#pragma mark - Private
#pragma Functions

- (void)programSelections {
    
    __weak typeof(self) weakSelf = self;
    
    [programSelectionClient programsFromResource: @"trainer-programs"
                                  withCompletion:^(NSArray<Program *> * _Nullable result, NSError * _Nullable error) {
        
        if (error == nil) {
            weakSelf.displayModels = [weakSelf displayModelsFrom: result];
            [weakSelf.delegate didUpdateProgramsWith: weakSelf.displayModels];
        } else {
            [weakSelf.delegate didFailToUpdateProgramsWith: error.localizedDescription];
        }

    }];
    
}

- (NSArray<ProgramDisplayModel*>*)displayModelsFrom:(NSArray<Program*>*)programs {
    
    NSMutableArray<ProgramDisplayModel*>* programsDisplayModel = [NSMutableArray array];
    
    for (Program* program in programs) {
        
        NSArray<AttributeDisplayModel*>* attributeDisplayModels;
        attributeDisplayModels = [self attributeDisplayModelsFrom: program.attributes];
        
        NSArray<NSString*>* imageNames;
        imageNames = [self intensityImageNamesFrom: program.attributes];
        
        NSArray<NSString*>* tags = [self tagsFrom: program.tags];
        
        ProgramDisplayModel* displayModel;
        displayModel = [[ProgramDisplayModel alloc] initWithNameText: program.name
                                                       trainWithText: [NSString stringWithFormat:@"with %@", program.trainer.name]
                                                            imageURL: program.imageURL
                                                           imageData: nil
                                                 intensityImageNames: imageNames
                                                          attributes: attributeDisplayModels
                                                                tags: tags];

        [programsDisplayModel addObject:displayModel];

    }
    
    return programsDisplayModel;
    
}

- (NSArray<NSString*>*)intensityImageNamesFrom:(NSArray<Attributes*>*)attributes {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"code == %i", AttributeCodeIntensity];
    Attributes* intensity = [attributes filteredArrayUsingPredicate:predicate].firstObject;
    
    NSMutableArray<NSString*>* imageNames = [NSMutableArray array];
    for (int i = 1; i <= intensity.total ; i++) {
        
        if (i <= intensity.value) {
            [imageNames addObject:@"sweat-drop-filled"];
        } else {
            [imageNames addObject:@"sweat-drop"];
        }
        
    }
    
    return imageNames;
    
}

- (NSArray<AttributeDisplayModel*>*)attributeDisplayModelsFrom:(NSArray<Attributes*>*)attributes {
    
    NSMutableArray<AttributeDisplayModel*>* displayModels = [NSMutableArray array];
    
    for (Attributes* attribute in attributes) {
        
        if (attribute.code == AttributeCodeIntensity) {
            continue;
        }
        
        float value = attribute.value / (float)attribute.total;

        AttributeDisplayModel* displaModel;
        displaModel = [[AttributeDisplayModel alloc] initWithNameText: attribute.name
                                                                value: value];
        [displayModels addObject: displaModel];
        
    }
    
    return displayModels;
}

- (NSArray<NSString*>*)tagsFrom:(NSArray<Tag*>*)tags {
    
    NSMutableArray<NSString*>* tagsDisplayModel = [NSMutableArray array];
    
    for (Tag* tag in tags) {
        [tagsDisplayModel addObject: tag.name];
    }
    
    return tagsDisplayModel;
    
}

- (void)imageDataForIndex:(NSUInteger)index {
    
    __weak typeof(self) weakSelf = self;
    NSString* imageURL = displayModels[index].imageURL;
    
    [assetDownloadClient
     downloadImageWithUrl:imageURL
     completion:^(NSData * _Nullable imageData) {
         
         ProgramDisplayModel* originalModel = weakSelf.displayModels[index];
         
         ProgramDisplayModel* newModel;
         newModel = [[ProgramDisplayModel alloc] initWithNameText: originalModel.nameText
                                                    trainWithText: originalModel.trainWithText
                                                         imageURL: originalModel.imageURL
                                                        imageData: imageData
                                              intensityImageNames: originalModel.intensityImageNames
                                                       attributes: originalModel.attributes
                                                             tags: originalModel.tags];
         
         NSMutableArray* originalDisplayModel = [NSMutableArray arrayWithArray: weakSelf.displayModels];
         originalDisplayModel[index] = newModel;
         
         weakSelf.displayModels = originalDisplayModel;
         
         [weakSelf.delegate didUpdateProgramImageWithIndex: index];
         
     }];
    
}

@end
