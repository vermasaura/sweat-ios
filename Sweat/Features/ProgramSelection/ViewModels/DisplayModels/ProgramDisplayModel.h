//
//  ProgramSelectionDisplayModel.h
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProgramScreenDisplayModel : NSObject

@property(readonly) NSString* screenLogo;
@property(readonly) NSString* screenTitle;

- (instancetype)initWithScreenLogo: (NSString*)logo screenTitle: (NSString*)title;

@end

@interface AttributeDisplayModel : NSObject

@property (readonly) NSString* nameText;
@property (readonly) float value;

- (instancetype)initWithNameText: (NSString* _Nullable)name value: (float)value;

@end

@interface ProgramDisplayModel : NSObject

@property(readonly) NSString* nameText;
@property(readonly) NSString* trainWithText;
@property(readonly) NSString* imageURL;
@property(readonly) NSData* imageData;
@property(readonly) NSArray <NSString*> *intensityImageNames;
@property(readonly) NSArray<AttributeDisplayModel*>* attributes;
@property(readonly) NSArray<NSString*> *tags;

- (instancetype)initWithNameText: (NSString*)name
                   trainWithText:(NSString*)trainWith
                        imageURL:(NSString*)url
                       imageData:(NSData* _Nullable)data
             intensityImageNames:(NSArray <NSString*> *)imageNames
                      attributes:(NSArray<AttributeDisplayModel*>* _Nullable)attributes
                            tags:(NSArray<NSString*> * _Nullable)tags;

@end

NS_ASSUME_NONNULL_END
