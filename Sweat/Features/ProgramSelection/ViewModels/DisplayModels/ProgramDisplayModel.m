//
//  ProgramDisplayModel.m
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import "ProgramDisplayModel.h"

@interface ProgramDisplayModel()

@property(nonatomic, strong) NSString* nameText;
@property(nonatomic, strong) NSString* trainWithText;
@property(nonatomic, strong) NSString* imageURL;
@property(nonatomic, strong) NSData* imageData;
@property(nonatomic, strong) NSArray <NSString*> *intensityImageNames;
@property(nonatomic, strong) NSArray<AttributeDisplayModel*>* attributes;
@property(nonatomic, strong) NSArray<NSString*> *tags;

@end

@implementation ProgramDisplayModel

- (instancetype)initWithNameText: (NSString*)name
                   trainWithText:(NSString*)trainWith
                        imageURL:(NSString*)url
                       imageData:(NSData* _Nullable)data
             intensityImageNames:(NSArray <NSString*> *)imageNames
                      attributes:(NSArray<AttributeDisplayModel*>* _Nullable)attributes
                            tags:(NSArray<NSString*> * _Nullable)tags {
    
    self = [super init];
    
    if (self) {
        self.nameText = name;
        self.trainWithText = trainWith;
        self.imageURL = url;
        self.imageData = data;
        self.intensityImageNames = imageNames;
        self.attributes = attributes;
        self.tags = tags;
    }
    
    return self;
    
}

@end

@interface AttributeDisplayModel()

@property (nonatomic, strong) NSString* nameText;
@property (nonatomic, assign) float value;

@end

@implementation AttributeDisplayModel

- (instancetype)initWithNameText: (NSString* _Nullable)name
                           value: (float)value {
    
    self = [super init];
    
    if (self) {
        self.nameText = name;
        self.value = value;
    }
    
    return self;
    
}

@end

@interface ProgramScreenDisplayModel()

@property(nonatomic, strong) NSString* screenLogo;
@property(nonatomic, strong) NSString* screenTitle;

@end

@implementation ProgramScreenDisplayModel

@synthesize screenLogo;
@synthesize screenTitle;

- (instancetype)initWithScreenLogo: (NSString*)logo screenTitle: (NSString*)title {
    
    self = [super init];
    
    if (self) {
        self.screenLogo = logo;
        self.screenTitle = title;
    }
    
    return self;
    
}

@end
