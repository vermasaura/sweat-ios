//
//  ProgressView.h
//  Sweat
//
//  Created by Saurabh Verma on 15/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProgressView : UIProgressView

@end

NS_ASSUME_NONNULL_END
