//
//  ProgressView.m
//  Sweat
//
//  Created by Saurabh Verma on 15/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import "ProgressView.h"

@implementation ProgressView

- (void)layoutSubviews
{
    [super layoutSubviews];
        
    [self.subviews
     enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.layer.masksToBounds = YES;
        obj.layer.cornerRadius =  self.bounds.size.height / 2.0;
        obj.layer.borderColor = [UIColor lightGrayColor].CGColor;
        obj.layer.borderWidth = 0.3;
    }];

}

@end
