//
//  ProgramSelectionTableViewCell.h
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProgramDisplayModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProgramSelectionTableViewCell : UITableViewCell

- (void)configureWithDisplayModel:(ProgramDisplayModel*)displayModel;

@end

NS_ASSUME_NONNULL_END
