//
//  ProgramSelectionTableViewCell.m
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import "ProgramSelectionTableViewCell.h"
#import "ProgressView.h"
#import "TagsView.h"
#import "Theme.h"

@interface ProgramSelectionTableViewCell()

@property(nonatomic, weak) IBOutlet UILabel* programName;
@property(nonatomic, weak) IBOutlet UILabel* trainWith;
@property(nonatomic, weak) IBOutlet UIView* containerView;
@property(nonatomic, weak) IBOutlet UIImageView* programImageView;
@property(nonatomic, weak) IBOutlet UIStackView* intensityStackView;
@property(nonatomic, weak) IBOutlet UIStackView* attributesStackView;
@property(nonatomic, weak) IBOutlet TagsView* tagsView;

@end

@implementation ProgramSelectionTableViewCell

@synthesize programName;
@synthesize trainWith;
@synthesize containerView;
@synthesize programImageView;
@synthesize intensityStackView;
@synthesize attributesStackView;
@synthesize tagsView;

- (void)prepareForReuse {
    
    [super prepareForReuse];
    
    programName.text = nil;
    trainWith.text = nil;
    programImageView.image = nil;
    
    [tagsView removeTags];
    
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    [self configureUI];
    
}

#pragma mark - Public
#pragma mark Functions

- (void)configureWithDisplayModel:(ProgramDisplayModel*)displayModel {
    
    programName.text = displayModel.nameText;
    trainWith.text = displayModel.trainWithText;
    programImageView.image = [UIImage imageWithData: displayModel.imageData];
    
    [self configureIntensityWith: displayModel.intensityImageNames];
    [self configureAttributesWith: displayModel.attributes];
    [self configureTagsWith: displayModel.tags];
    
}

#pragma mark - Private
#pragma mark Functions

- (void)configureUI {
    
    containerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    containerView.layer.borderWidth = 0.3;
    containerView.layer.cornerRadius = 5.0;
    containerView.layer.shadowRadius = 1.5;
    containerView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    containerView.layer.shadowOpacity = 0.9;
    containerView.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    
    programName.font = [UIFont fontWithName:[UIFont montserratBoldFont] size: 20.0];
    programName.textColor = [UIColor roseColor];
    
    trainWith.font = [UIFont fontWithName:[UIFont openSansBold] size:17.0];
    trainWith.textColor = [UIColor grayColor];
    
}

- (void)configureIntensityWith:(NSArray<NSString*>*)imageNames {
    
    [intensityStackView.arrangedSubviews
     enumerateObjectsUsingBlock:
     ^(UIImageView* imageView, NSUInteger index, BOOL *stop) {
         imageView.image = [UIImage imageNamed: imageNames[index]];
     }];

}

- (void)configureAttributesWith:(NSArray<AttributeDisplayModel*>*)displayModels {
    
    [attributesStackView.arrangedSubviews
     enumerateObjectsUsingBlock:
     ^(UIView* subView, NSUInteger index, BOOL *stop) {
         
         if ([subView isKindOfClass:[UIStackView class]]) {
             
             UIStackView* stackView = (UIStackView*)subView;
             
             for (UIView *subView in stackView.arrangedSubviews) {
                 
                 if ([subView isKindOfClass:[UILabel class]]) {
                     
                     UILabel* attribute = (UILabel*)subView;
                     attribute.text = displayModels[index].nameText;
                     attribute.textColor = [UIColor grayColor];
                     attribute.font = [UIFont fontWithName:[UIFont openSansSemiBold] size:14.0];
                     
                 } else if([subView isKindOfClass:[UIProgressView class]]) {
                     
                     ProgressView* progressView = (ProgressView*)subView;
                     progressView.progress = displayModels[index].value;
                     progressView.progressTintColor = [UIColor roseColor];
                     
                 }
                 
             }
             
         }
         
     }];
    
}

- (void)configureTagsWith:(NSArray<NSString*>*)tags {
    
    [tagsView addTags: tags];
    
}

@end
