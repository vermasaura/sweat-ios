//
//  TagsView.h
//  Sweat
//
//  Created by Saurabh Verma on 16/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TagsView : UIView

- (void)addTags:(NSArray<NSString*>*)tags;
- (void)removeTags;

@end

NS_ASSUME_NONNULL_END
