//
//  TagsView.m
//  Sweat
//
//  Created by Saurabh Verma on 16/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import "TagsView.h"

@implementation TagsView

- (void)layoutSubviews {
    
    [super layoutSubviews];
    self.translatesAutoresizingMaskIntoConstraints = false;

}

static CGFloat margin = 5.0;
static CGFloat offset = 10.0;

#pragma mark - Public
#pragma mark Functions

- (void)addTags:(NSArray<NSString*>*)tags {
    
    __block CGFloat x = 0.0;
    __block CGFloat y = margin;
    __weak typeof(self) weakSelf = self;
    
    [tags enumerateObjectsUsingBlock:
     ^(NSString* tag, NSUInteger index, BOOL *stop) {
        
         UIButton *button = [weakSelf buttonWithTitle: tag];
         [weakSelf addSubview: button];
         [[button.leftAnchor constraintEqualToAnchor: weakSelf.leftAnchor constant: x] setActive: true];
         if (index == tags.count - 1) {
             [[button.bottomAnchor constraintEqualToAnchor: weakSelf.bottomAnchor constant: -margin] setActive: true];
             [[button.topAnchor constraintEqualToAnchor: weakSelf.topAnchor constant: y] setActive: true];
         } else {
             [[button.topAnchor constraintEqualToAnchor: weakSelf.topAnchor constant: y] setActive: true];
         }
         
         CGFloat buttonWidth = [weakSelf sizeFromTitle: tags[index]].width + offset;
         x += buttonWidth + margin;
         
         CGFloat nextButtonWidth = index <= tags.count - 2 ? [weakSelf sizeFromTitle: tags[index + 1]].width + offset : buttonWidth;
         
         CGFloat buttonHeight = [weakSelf sizeFromTitle: tags[index]].height + offset;
         if (x + nextButtonWidth + margin >= weakSelf.bounds.size.width) {
             x = 0.0;
             y += buttonHeight + margin;
         }
         
     }];
    
}

- (void)removeTags {
    
    for (UIView* subView in self.subviews) {
        [subView removeFromSuperview];
    }
    
}

#pragma mark - Private
#pragma mark Functions

- (CGSize)sizeFromTitle:(NSString*)title {
    
    UIFont* font = [UIFont systemFontOfSize: 10.0];
    return [title sizeWithAttributes: @{NSFontAttributeName: font}];
    
}

- (UIButton*)buttonWithTitle:(NSString*)title {
    
    CGSize size = [self sizeFromTitle:title];
    UIButton* button = [UIButton buttonWithType: UIButtonTypeCustom];
    [[button titleLabel] setFont: [UIFont systemFontOfSize: 10.0]];
    [button setTitle: title forState: UIControlStateNormal];
    [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    button.layer.cornerRadius = size.height / 2.0;
    button.layer.borderWidth = 0.5;
    button.layer.borderColor = [UIColor grayColor].CGColor;
    
    button.translatesAutoresizingMaskIntoConstraints = false;
    [[button.widthAnchor constraintEqualToConstant: size.width + offset] setActive: true];
    [[button.heightAnchor constraintEqualToConstant: size.height + offset] setActive: true];

    return button;

}

@end
