//
//  ProgramSelectionRouter.h
//  Sweat
//
//  Created by Saurabh Verma on 15/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ProgramDisplayModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ProgramSelectionRouterType <NSObject>

- (void)presentProgramDetailScreenWith:(ProgramDisplayModel*)programDisplayModel;

@end

@interface ProgramSelectionRouter : NSObject <ProgramSelectionRouterType>

@property(nonatomic, weak) UIViewController* hostViewController;

@end

NS_ASSUME_NONNULL_END
