//
//  ProgramSelectionRouter.m
//  Sweat
//
//  Created by Saurabh Verma on 15/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import "ProgramSelectionRouter.h"

@implementation ProgramSelectionRouter

@synthesize hostViewController;

#pragma mark - ProgramSelectionRouterType Conformance

- (void)presentProgramDetailScreenWith:(ProgramDisplayModel *)programDisplayModel {
    
    // Create the Stack for Program Detail Screen with all the dependecies
    // Then push the Detail screen on hostViewController
    
//    [hostViewController.navigationController pushViewController:<#(nonnull UIViewController *)#> animated:<#(BOOL)#>];

}

@end
