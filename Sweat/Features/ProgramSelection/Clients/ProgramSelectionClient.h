//
//  ProgramSelectionClient.h
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Program.h"
#import "BundleProtocol.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^ProgramResponse)(NSArray<Program*>* _Nullable result, NSError* _Nullable error);

@protocol ProgramSelectionClientType <NSObject>

- (void)programsFromResource:(NSString*)resource withCompletion: (ProgramResponse)completion;

@end

@interface ProgramSelectionClient : NSObject <ProgramSelectionClientType>

- (instancetype)initWithBundle:(id<BundleProtocol>)bundle;

@end

NS_ASSUME_NONNULL_END
