//
//  NSBundle+BundleProtocol.h
//  Sweat
//
//  Created by Saurabh Verma on 16/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//
#import "BundleProtocol.h"

#ifndef NSBundle_BundleProtocol_h
#define NSBundle_BundleProtocol_h

@interface NSBundle() <BundleProtocol>

@end

#endif /* NSBundle_BundleProtocol_h */
