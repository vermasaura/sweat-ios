//
//  NSURLSession+URLSessionProtocol.h
//  Sweat
//
//  Created by Saurabh Verma on 14/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#ifndef NSURLSession_URLSessionProtocol_h
#define NSURLSession_URLSessionProtocol_h

@interface NSURLSession() <URLSessionProtocol>

@end

#endif /* NSURLSession_URLSessionProtocol_h */
