//
//  NSURLSessionDownloadTask+URLSessionDownloadProtocol.h
//  Sweat
//
//  Created by Saurabh Verma on 14/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#ifndef NSURLSessionDownloadTask_URLSessionDownloadProtocol_h
#define NSURLSessionDownloadTask_URLSessionDownloadProtocol_h

@interface NSURLSessionDownloadTask() <URLSessionDownloadProtocol>

@end

#endif /* NSURLSessionDownloadTask_URLSessionDownloadProtocol_h */
