//
//  AssetDownloadClient.m
//  Sweat
//
//  Created by Saurabh Verma on 12/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import "AssetDownloadClient.h"

@interface AssetDownloadClient()

@property(nonatomic, strong) id<URLSessionProtocol> session;

@end

@implementation AssetDownloadClient
@synthesize session;

#pragma mark - Initialization

- (instancetype)initWithSession: (id<URLSessionProtocol>)session {
    
    self = [super init];
    if (self) {
        self.session = session;
    }
    return self;
    
}

#pragma mark - AssetDownloadClientType Conformance

- (void)downloadImageWithUrl: (NSString*)url completion: (void (^)(NSData* _Nullable imageData))completion {
    
    NSURLSessionDownloadTask *task;
    task = (NSURLSessionDownloadTask*)
    [session
     downloadTaskWithURL:[NSURL URLWithString: url]
     completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
         
         if (error != nil) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 completion(nil);
             });
             return;
         }
         
         NSData *data = [NSData dataWithContentsOfURL:location];
         
         dispatch_async(dispatch_get_main_queue(), ^{
             completion(data);
         });
         
     }];
    
    [task resume];

}

@end
