//
//  URLSessionDownloadProtocol.h
//  Sweat
//
//  Created by Saurabh Verma on 14/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#ifndef URLSessionDownloadProtocol_h
#define URLSessionDownloadProtocol_h

@protocol URLSessionDownloadProtocol <NSObject>

- (void)resume;

@end

#endif /* URLSessionDownloadProtocol_h */
