//
//  URLSessionProtocol.h
//  Sweat
//
//  Created by Saurabh Verma on 14/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//
#import "URLSessionDownloadProtocol.h"

#ifndef URLSessionProtocol_h
#define URLSessionProtocol_h

@protocol URLSessionProtocol <NSObject>

- (id<URLSessionDownloadProtocol>)downloadTaskWithURL:(NSURL *)url completionHandler:(void (^)(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error))completionHandler;

@end

#endif /* URLSessionProtocol_h */
