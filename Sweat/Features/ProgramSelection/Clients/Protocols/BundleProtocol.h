//
//  BundleProtocol.h
//  Sweat
//
//  Created by Saurabh Verma on 16/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#ifndef BundleProtocol_h
#define BundleProtocol_h

@protocol BundleProtocol <NSObject>

- (nullable NSString *)pathForResource:(nullable NSString *)name ofType:(nullable NSString *)ext;

@end

#endif /* BundleProtocol_h */
