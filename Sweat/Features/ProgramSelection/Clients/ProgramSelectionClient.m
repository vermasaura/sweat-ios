//
//  ProgramSelectionClient.m
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import "ProgramSelectionClient.h"

@interface ProgramSelectionClient()

@property(nonatomic, strong) id<BundleProtocol> bundle;

@end

@implementation ProgramSelectionClient
@synthesize bundle;

- (instancetype)initWithBundle:(id<BundleProtocol>)bundle {
    
    self = [super init];
    if (self) {
        self.bundle = bundle;
    }
    return self;

}

#pragma mark - ProgramSelectionClientType Conformance

- (void)programsFromResource:(NSString*)resource withCompletion: (ProgramResponse)completion {
    
    __block NSError* error = nil;
    NSString *path = [bundle pathForResource: resource ofType: @"json"];
    
    if (path == nil) {
        error = [self errorForResourceNotFound];
        completion(nil, error);
        return;
    }
    dispatch_queue_t queue = dispatch_queue_create("ProgramSelectionClientQueue", DISPATCH_QUEUE_SERIAL);

    dispatch_async(queue, ^{
       
        NSData *data = [NSData dataWithContentsOfFile: path
                                              options: NSDataReadingMappedAlways
                                                error: &error];
        if (error) {
            error = [self errorForDataNotReadable];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(nil, error);
                return;
            });
            
        }
        
        NSArray* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error: &error];
        
        if (error) {
            error = [self errorForBadJSON];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(nil, error);
            });
            
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion([self programsFromJson: json], nil);
            });
        }

    });

}

#pragma mark - Private
#pragma mark Functions

- (NSArray<Program*>*)programsFromJson:(NSArray*)json {

    NSMutableArray<Program*>* programs = [NSMutableArray array];
    
    for (NSDictionary* dictionary in json) {

        if (dictionary[@"id"] == nil || ![dictionary[@"id"] isKindOfClass: [NSNumber class]]) {
            // Ignore the Program and continue with next object
            continue;
        }
        
        Program *program;
        program = [[Program alloc] initWithId: [dictionary[@"id"] integerValue]
                                      acronym: dictionary[@"acronym"]
                                     codeName: dictionary[@"code_name"]
                                         name: dictionary[@"name"]
                                     imageURL: dictionary[@"image"]
                                   attributes: [self attributesFromJson: dictionary[@"attributes"]]
                                         tags: [self tagsFromJson:dictionary[@"tags"]]
                                      trainer: [self trainerFromJson:dictionary[@"trainer"]]];
        
        [programs addObject:program];
        
    }
    
    return programs;

}

- (NSArray<Attributes*>*)attributesFromJson:(NSArray*)json {
    
    NSMutableArray<Attributes*>* attributes = [NSMutableArray array];
    
    for (NSDictionary* dictionary in json) {
        
        if (dictionary[@"id"] == nil || ![dictionary[@"id"] isKindOfClass: [NSNumber class]]) {
            continue;
        }
        
        Attributes *attribute;
        AttributeCode code = [dictionary[@"code_name"] isEqualToString:@"intensity"] ? AttributeCodeIntensity : AttributeCodeDefault;
        attribute = [[Attributes alloc] initWithId: [dictionary[@"id"] integerValue]
                                              code: code
                                              name: dictionary[@"name"]
                                             value: [dictionary[@"value"] integerValue]
                                             total: [dictionary[@"total"] integerValue]];
        
        [attributes addObject:attribute];
        
    }
    
    return attributes;
    
}

- (Trainer*)trainerFromJson:(NSDictionary*)json {
    
    if (json[@"id"] == nil || ![json[@"id"] isKindOfClass: [NSNumber class]]) {
        return nil;
    }
    
    Trainer* trainer;
    trainer = [[Trainer alloc] initWithId: [json[@"id"] integerValue]
                                          codeName: json[@"code_name"]
                                              name: json[@"name"]
                                          imageURL: json[@"image_url"]];
    
    return trainer;

}

- (NSArray<Tag*>*)tagsFromJson:(NSArray*)json {
    
    NSMutableArray<Tag*>* tags = [NSMutableArray array];
    
    for (NSDictionary* dictionary in json) {
        
        if (dictionary[@"id"] == nil || ![dictionary[@"id"] isKindOfClass: [NSNumber class]]) {
            continue;
        }
        
        Tag *tag;
        tag = [[Tag alloc] initWithId:[dictionary[@"id"] integerValue] name:dictionary[@"name"]];
        [tags addObject:tag];
        
    }
    
    return tags;
    
}

- (NSError*)errorForResourceNotFound {
    
    return [NSError errorWithDomain: @"ResourceNotFound"
                               code: 1001
                           userInfo: @{@"ErrorMesaage": @"Resource not found"}];

}

- (NSError*)errorForDataNotReadable {
    
    return [NSError errorWithDomain: @"DataNotReadable"
                               code: 1002
                           userInfo: @{@"ErrorMesaage": @"Coudn't read data from resource"}];
    
}

- (NSError*)errorForBadJSON {
    
    return [NSError errorWithDomain: @"BadJSON"
                               code: 1003
                           userInfo: @{@"ErrorMesaage": @"Coudn't read JSON from data"}];

}

@end
