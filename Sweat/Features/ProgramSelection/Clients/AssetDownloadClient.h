//
//  AssetDownloadClient.h
//  Sweat
//
//  Created by Saurabh Verma on 12/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "URLSessionProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@protocol AssetDownloadClientType <NSObject>

- (void)downloadImageWithUrl: (NSString*)url completion: (void (^)(NSData* _Nullable imageData))completion;

@end

@interface AssetDownloadClient : NSObject <AssetDownloadClientType>

- (instancetype)initWithSession: (id<URLSessionProtocol>)session;

@end

NS_ASSUME_NONNULL_END
