//
//  AppDelegate.m
//  Sweat
//
//  Created by Saurabh Verma on 10/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import "AppDelegate.h"
#import "ProgramSelectionViewController.h"
#import "ProgramSelectionViewModel.h"
#import "ProgramSelectionClient.h"
#import "NSURLSession+URLSessionProtocol.h"
#import "NSBundle+BundleProtocol.h"

@interface AppDelegate ()
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSBundle* bundle = [NSBundle mainBundle];
    ProgramSelectionClient* programSelectionClient = [[ProgramSelectionClient alloc] initWithBundle: bundle];
    
    NSURLSession* session = [NSURLSession sharedSession];
    AssetDownloadClient* assetDownloadClient = [[AssetDownloadClient alloc] initWithSession: session];
    
    ProgramSelectionRouter* router = [[ProgramSelectionRouter alloc] init];
    ProgramSelectionViewModel* viewModel = [[ProgramSelectionViewModel alloc] initWithProgramSelectionClient: programSelectionClient assetDownloadClient:assetDownloadClient router:router];
    
    ProgramSelectionViewController* viewController = [[ProgramSelectionViewController alloc] initWithViewModel: viewModel];
    viewModel.delegate = viewController;
    router.hostViewController = viewController;
    
    UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController: viewController];
    
    _window.rootViewController = navigationController;
    
    [[UINavigationBar appearance] setBarStyle:UIBarStyleDefault];
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    
    return YES;

}

@end
