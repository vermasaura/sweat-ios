//
//  Theme.h
//  Sweat
//
//  Created by Saurabh Verma on 18/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface UIColor (Sweat)

// http://chir.ag/projects/name-that-color/#F50272
+ (UIColor*)roseColor;

@end

@interface UIFont (Sweat)

+ (NSString*)montserratBoldFont;
+ (NSString*)openSansBold;
+ (NSString*)openSansSemiBold;

@end

