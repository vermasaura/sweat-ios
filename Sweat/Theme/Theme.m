//
//  Theme.m
//  Sweat
//
//  Created by Saurabh Verma on 18/7/19.
//  Copyright © 2019 Sweat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Theme.h"

@implementation UIColor (Sweat)

+ (UIColor*)roseColor {
    return [UIColor colorWithRed: 245/255.0 green: 2/255.0 blue: 114/255.0 alpha: 1.0];
}

@end

@implementation UIFont (Sweat)

+ (NSString*)montserratBoldFont {
    return @"Montserrat-Bold";
}

+ (NSString*)openSansBold {
    return @"OpenSans-Bold";
}

+ (NSString*)openSansSemiBold {
    return @"OpenSans-Semibold";
}

@end
